# Readme

## Running the project

The project uses carthage. So we need to run the following command to install the libraries 

```
carthage update --platform iOS
```

## Features

* Allows user to create a task
* Schedule a task for a particular day
* Mark a task as complete
* See all completed tasks
* Edit a task and change title or schedule time