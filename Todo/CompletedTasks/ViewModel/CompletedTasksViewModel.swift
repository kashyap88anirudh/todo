//
//  CompletedTasksViewModel.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 28/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation

class CompletedTasksViewModel {
  
  private let taskDataStore: TaskDataStore
  private var completedTasks: [Task] = []
  
  var numberOfCompletedTasks: Int {
    return completedTasks.count
  }
  
  init(taskDataStore: TaskDataStore = TaskDataStore()) {
    self.taskDataStore = taskDataStore
    completedTasks = taskDataStore.completedTasks()
  }
  
  func cellViewModel(indexPath: IndexPath) -> TaskCellViewModel? {
    guard indexPath.row < completedTasks.count else { return nil }
    return TaskCellViewModel(task: completedTasks[indexPath.row])
  }
  
}
