//
//  CompletedTasksViewController.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 28/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation
import UIKit

class CompletedTasksViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var emptyStateLabel: UILabel!
  
  let viewModel: CompletedTasksViewModel =  CompletedTasksViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
     setupView()
  }
  
  //MARK:- Private
  private func setupView() {
    view.backgroundColor = .background
    
    tableView.dataSource = self
    tableView.backgroundColor = .clear
    tableView.tableFooterView = UIView()
    tableView.separatorStyle = .none
    tableView.allowsSelection = false
    
    // Do any additional setup after loading the view, typically from a nib.
    tableView.registerNib(TaskCell.self)
    
    emptyStateLabel.isHidden = viewModel.numberOfCompletedTasks > 0
  }
  
  //MARK:- Actions

  @IBAction func doneButtonAction(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
}

//MARK:- Table view data source

extension CompletedTasksViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfCompletedTasks
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueCell(type: TaskCell.self, for: indexPath)
    if let cellViewModel = viewModel.cellViewModel(indexPath: indexPath) {
      cell.bind(cellViewModel)
    }
    return cell
  }
  
}
