//
//  TableViewExtensions.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 26/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell: HasReusableIdentifier { }
extension UITableViewHeaderFooterView: HasReusableIdentifier { }

extension UITableView {
  func registerNib<T: UITableViewCell>(_ : T.Type) {
    let nib = UINib(nibName: T.reusableIdentifier, bundle: nil)
    self.register(nib, forCellReuseIdentifier: T.reusableIdentifier)
  }
  
  func registerNibForHeaderFooter<T: UITableViewHeaderFooterView>(_ type: T.Type) {
    let nib = UINib(nibName: T.reusableIdentifier, bundle: nil)
    register(nib, forHeaderFooterViewReuseIdentifier: T.reusableIdentifier)
  }
  
  func dequeueCell<T: UITableViewCell>(type: T.Type, for indexPath: IndexPath) -> T {
    guard let cell = dequeueReusableCell(withIdentifier: T.reusableIdentifier, for: indexPath) as? T else {
      fatalError("Could not dequeu cell with identifier \(T.reusableIdentifier)")
    }
    return cell
  }
  
  func dequeueCellHeaderFooterView<T: UITableViewHeaderFooterView>(type: T.Type) -> T {
    guard let cell = dequeueReusableHeaderFooterView(withIdentifier: T.reusableIdentifier) as? T else {
      fatalError("Could not dequeu header footer view with identifier \(T.reusableIdentifier)")
    }
    return cell
  }
}
