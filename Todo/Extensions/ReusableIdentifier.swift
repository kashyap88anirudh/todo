//
//  ReusableIdentifier.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 26/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation
import UIKit

protocol HasReusableIdentifier {
  static var reusableIdentifier: String { get }
}

extension HasReusableIdentifier where Self: UIView {
  static var reusableIdentifier: String {
    return String(describing: self).components(separatedBy: ".").last!
  }
}
