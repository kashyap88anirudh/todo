//
//  DateExtensions.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 27/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation

extension Date {
  
  var startOfDay: Date? {
    let calendar = Calendar.current
    return calendar.startOfDay(for: self)
  }
  
  var endOfDay: Date? {
    let calendar = Calendar.current
    var endOfToday = calendar.dateComponents([.day,.month,.year], from: self)
    endOfToday.setValue(23, for: .hour)
    endOfToday.setValue(59, for: .minute)
    endOfToday.setValue(59, for: .second)
    return calendar.date(from: endOfToday)
  }
}
