//
//  ViewController.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 25/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import UIKit

class SummaryViewController: UIViewController {

  lazy var viewModel: SummaryViewModel = SummaryViewModel(viewProtocol: self)
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var emptyStateView: UIView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewModel.willStartShowingView()
    tableView.reloadData()
    emptyStateView.isHidden = !(viewModel.sectionsCount == 0)
  }
  
  
  //MARK:- Private
  private func setupView() {
    let titleView = UIImageView(image: UIImage(named: "navTitle")!)
    titleView.frame = CGRect(x: 0, y: 0, width: 71, height: 18)
    navigationItem.titleView = titleView
    
    view.backgroundColor = .background
    
    tableView.dataSource = self
    tableView.delegate = self
    tableView.backgroundColor = .clear
    tableView.tableFooterView = UIView()
    tableView.separatorStyle = .none
    
    // Do any additional setup after loading the view, typically from a nib.
    tableView.registerNib(TaskCell.self)
    tableView.registerNibForHeaderFooter(SummaryTableHeaderCell.self)
  }

  //MARK:- Actions
  @IBAction func addNewTaskAction() {
    let storyboard = UIStoryboard(name: "AddEditTask", bundle: nil)
    let navController = storyboard.instantiateInitialViewController() as! UINavigationController
    let addEditTaskViewController  = navController.topViewController as! AddEditTaskViewController
    addEditTaskViewController.viewModel = AddEditTaskViewModel(viewProtocol: addEditTaskViewController)
    
    present(navController, animated: true, completion: nil)
  }
  
  @IBAction func showCompletedAction(_ sender: Any) {
    let storyboard = UIStoryboard(name: "CompletedTasks", bundle: nil)
    let addEditTaskViewController = storyboard.instantiateInitialViewController()!
    present(addEditTaskViewController, animated: true, completion: nil)
  }
}


extension SummaryViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfTasks(inSection: section)
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.sectionsCount
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueCell(type: TaskCell.self, for: indexPath)
    if let cellViewModel = viewModel.cellViewModel(indexPath: indexPath) {
      cell.bind(cellViewModel)
    }
    cell.completeActionCallback = {
      cell.animateCompletion { [weak self] in
        self?.viewModel.completedTask(atIndex: indexPath)
      }
    }
    return cell
  }
  
}

extension SummaryViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = tableView.dequeueCellHeaderFooterView(type: SummaryTableHeaderCell.self)
    header.titleLabel.text = viewModel.titleForSection(section)
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 48.0
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    viewModel.editTasks(at: indexPath)
  }
}

extension SummaryViewController: SummaryViewProtocol {
  func reloadData() {
    tableView.reloadData()
    emptyStateView.isHidden = !(viewModel.sectionsCount == 0)
  }
  
  func showEditView(for task: Task) {
    let storyboard = UIStoryboard(name: "AddEditTask", bundle: nil)
    let navController = storyboard.instantiateInitialViewController() as! UINavigationController
    let addEditTaskViewController  = navController.topViewController as! AddEditTaskViewController
    addEditTaskViewController.viewModel = AddEditTaskViewModel(viewProtocol: addEditTaskViewController, taskToUpdate: task)
    
    present(navController, animated: true, completion: nil)
  }
  
}
