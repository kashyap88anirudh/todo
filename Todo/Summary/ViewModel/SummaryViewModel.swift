//
//  SummaryViewModel.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 27/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation

protocol SummaryViewProtocol: class {
  func reloadData()
  func showEditView(for task: Task)
}

class SummaryViewModel {
  
  enum Sections {
    case today, upcoming
  }
  
  let taskDataStore: TaskDataStore
  
  private var sectionsToDisplay: [Sections] = []
  private var tasksForToday: [Task] = []
  private var upcomingTasks: [Task] = []
  
  private weak var viewProtocol: SummaryViewProtocol?
  
  var sectionsCount: Int {
    return sectionsToDisplay.count
  }
  
  init(taskDataStore: TaskDataStore = TaskDataStore(),
       viewProtocol: SummaryViewProtocol?) {
    self.taskDataStore = taskDataStore
    self.viewProtocol = viewProtocol
  }
  
  func willStartShowingView() {
    refreshTasks()
  }
  
  func numberOfTasks(inSection index: Int) -> Int{
    if index < sectionsToDisplay.count {
      let section = sectionsToDisplay[index]
      switch section {
      case .today:
        return tasksForToday.count
      case .upcoming:
        return upcomingTasks.count
      }
    }
    
    return 0
  }
  
  func cellViewModel(indexPath: IndexPath) -> TaskCellViewModel? {
    guard let task = task(at: indexPath) else { return nil }
    return TaskCellViewModel(task: task)
  }
  
  func titleForSection(_ section: Int) -> String {
    guard section < sectionsToDisplay.count else { return "" }
    
    switch sectionsToDisplay[section] {
    case .today:
      return "Today"
    case .upcoming:
      return "Upcoming"
    }
  }
  
  func completedTask(atIndex indexPath: IndexPath) {
    guard let task = task(at: indexPath) else { return }
    taskDataStore.complete(task: task)
    refreshTasks()
    viewProtocol?.reloadData()
  }
  
  func editTasks(at indexPath: IndexPath) {
    guard let task = task(at: indexPath) else { return }
    viewProtocol?.showEditView(for: task)
  }
  
  //MARK:- Private
  
  private func task(at indexPath: IndexPath) -> Task? {
    guard indexPath.section < sectionsToDisplay.count else { return nil }
    
    switch sectionsToDisplay[indexPath.section] {
    case .today:
      guard indexPath.row < tasksForToday.count else { return nil }
      return tasksForToday[indexPath.row]
    case .upcoming:
      guard indexPath.row < upcomingTasks.count else { return nil }
      return upcomingTasks[indexPath.row]
    }
  }
  private func updateTasks() {
    tasksForToday = taskDataStore.tasksForToday()
    upcomingTasks = taskDataStore.upcomingTasks()
  }
  
  private func setupSections() {
    sectionsToDisplay = []
    let hasTasksForToday = taskDataStore.tasksForToday().count > 0
    let hasUpcomingTasks = taskDataStore.upcomingTasks().count > 0
    if hasTasksForToday {
      sectionsToDisplay.append(.today)
    }
    
    if hasUpcomingTasks {
      sectionsToDisplay.append(.upcoming)
    }
  }

  private func refreshTasks() {
    updateTasks()
    setupSections()
  }
  
}
