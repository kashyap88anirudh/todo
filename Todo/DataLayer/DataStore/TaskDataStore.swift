//
//  TodoDataStore.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 26/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation

class TaskDataStore {
  
  private let todoDatabaseClient: TaskDatabaseClient
  
  init( todoDatabaseClient: TaskDatabaseClient = TaskDatabaseClient()) {
    self.todoDatabaseClient = todoDatabaseClient
  }
  
  func add(_ task: Task) {
    todoDatabaseClient.add(task: task)
  }
  
  func complete(task: Task) {
    var task = task
    task.isComplete = true
    update(task)
  }
  
  func update(_ task: Task) {
    todoDatabaseClient.update(task: task)
  }
  
  func tasksForToday() -> [Task] {
    return todoDatabaseClient.itemsScheduledFor(date: Date()) ?? []
  }
  
  func upcomingTasks() -> [Task] {
    let itemsScheduledLater =  todoDatabaseClient.itemsScheduledAfter(date: Date()) ?? []
    let unscheduled = todoDatabaseClient.itemsWithNoSchedule() ?? []
    return itemsScheduledLater + unscheduled
  }
  
  func completedTasks() -> [Task] {
    return todoDatabaseClient.completedItems() ?? []
  }
}
