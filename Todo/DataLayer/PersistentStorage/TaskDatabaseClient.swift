//
//  TodoDatabaseClient.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 26/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation
import RealmSwift

class TaskDatabaseClient {
  
  var realm: Realm? {
    do {
      let realm = try Realm()
      return realm
    } catch(let error as NSError) {
      debugPrint("Error accessing realm : \(error)")
      return nil
    }
  }
  
  func add(task: Task) {
    var taskToAdd = task
    taskToAdd.taskID = UUID().uuidString
    try? realm?.write {
      realm?.add(taskToAdd.realmObject)
    }
  }
  
  func update(task: Task) {
    
    guard
      realm?.objects(TaskRealm.self).filter("taskID == %@", task.taskID).first != nil
      else { return }
    
    try? realm?.write {
      realm?.add(task.realmObject, update: true)
    }
  }
  
  func allItems() -> [Task]? {
    guard let tasks = realm?.objects(TaskRealm.self) else { return nil }
    return tasks.map { (taskRealm) -> Task in
      taskRealm.task
    }
  }
  
  func itemsScheduledFor(date: Date) -> [Task]? {
    guard let startOfDay = date.startOfDay,
    let endOfDay = date.endOfDay else { return nil }
    
    let todayPredicateQuery = "schedule >= %@ AND schedule <= %@ AND isComplete = %@"
    guard let realmTasks = realm?.objects(TaskRealm.self).filter(todayPredicateQuery, startOfDay, endOfDay, NSNumber(value: false))
      else { return nil }
    
    return realmTasks.map { (realmTask) -> Task in
      realmTask.task
    }

  }
  
  func itemsScheduledAfter(date: Date) -> [Task]? {
    guard let endOfDay = date.endOfDay else { return nil }
    
    guard let realmTasks = realm?.objects(TaskRealm.self).filter("schedule > %@ AND isComplete = %@", endOfDay, NSNumber(value: false))
      else { return nil }
    
    return realmTasks.map { (realmTask) -> Task in
      realmTask.task
    }
  }
  
  func itemsWithNoSchedule() -> [Task]? {
    guard let realmTasks = realm?.objects(TaskRealm.self).filter("schedule = nil AND isComplete = %@", NSNumber(value: false))
      else { return nil }
    
    return realmTasks.map { (realmTask) -> Task in
      realmTask.task
    }
  }
  
  func completedItems() -> [Task]? {
    guard let realmTasks = realm?.objects(TaskRealm.self).filter("isComplete = %@", NSNumber(value: true))
      else { return nil }
    
    return realmTasks.map { (realmTask) -> Task in
      realmTask.task
    }
  }
}

//Database domain object.
class TaskRealm: Object {
  @objc dynamic var title: String = ""
  @objc dynamic var schedule: Date?
  @objc dynamic var isComplete: Bool = false
  @objc dynamic var taskID: String = ""
  @objc dynamic var createdOn: Date = Date()
  
  override class func primaryKey() -> String {
    return "taskID"
  }
}

private extension Task {
  var realmObject: TaskRealm {
    let object = TaskRealm()
    object.title = title
    object.schedule = schedule
    object.isComplete = isComplete
    object.createdOn = createdOn
    object.taskID = taskID
    return object
  }
}

private extension TaskRealm {
  var task: Task {
    return Task(title: title, schedule: schedule,
                isComplete: isComplete, createdOn: createdOn,
                taskID: taskID)
  }
}
