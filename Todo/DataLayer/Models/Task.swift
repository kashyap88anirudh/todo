//
//  TodoItem.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 26/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation

struct Task {
  var title: String
  var schedule: Date?
  var isComplete: Bool
  var createdOn: Date
  var taskID: String
  
  init(title: String, schedule: Date?,
       isComplete: Bool, createdOn: Date,
       taskID: String = "") {
    self.title = title
    self.schedule = schedule
    self.isComplete = isComplete
    self.createdOn = createdOn
    self.taskID = taskID
  }
}
