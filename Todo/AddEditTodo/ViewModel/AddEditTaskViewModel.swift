//
//  AddEditTaskViewModel.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 27/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation

protocol AddEditTaskViewProtocol: class {
  func updateSave(enabled: Bool)
  func enableSchedule(_ enable: Bool)
  func updateScheduleDate(_ date: Date)
  func updateTaskTitle(_ title: String)
}

class AddEditTaskViewModel {
  private var currentTask: Task?
  private let taskDataStore: TaskDataStore
  
  private var taskTitleString: String?
  private var scheduleDate: Date?
  private var scheduleEnabled: Bool = false
  
  private weak var viewProtocol: AddEditTaskViewProtocol?
  
  var viewTitleText: String {
    return currentTask == nil ? "Add Task" : "Update Task"
  }
  
  init(taskDataStore: TaskDataStore = TaskDataStore(),
       viewProtocol: AddEditTaskViewProtocol,
       taskToUpdate: Task? = nil) {
    self.taskDataStore = taskDataStore
    self.viewProtocol = viewProtocol
    self.currentTask = taskToUpdate
    
    taskTitleString = taskToUpdate?.title
    scheduleDate = taskToUpdate?.schedule
  }
  
  func viewWillStartShowing() {
    viewProtocol?.updateSave(enabled: taskTitleString?.count ?? 0 > 0)
    viewProtocol?.enableSchedule(currentTask?.schedule != nil)
    
    if let scheduleDate = currentTask?.schedule {
      viewProtocol?.updateScheduleDate(scheduleDate)
    }
    
    if let title = taskTitleString {
      viewProtocol?.updateTaskTitle(title)
    }

  }

  func addOrUpdateTask() {
    guard let title = taskTitleString else { return }
    
    if currentTask == nil {
      currentTask = Task(title: title, schedule: scheduleDate, isComplete: false, createdOn: Date())
      add(currentTask!)
    } else if var exisitingTask = currentTask {
      exisitingTask.title = title
      exisitingTask.schedule = scheduleDate
      update(exisitingTask)
    }
  }
  
  func titleUpdated(_ title: String) {
    taskTitleString = title
    viewProtocol?.updateSave(enabled: title.count > 0)
  }
  
  func scheduleDateUpdated(_ date: Date) {
    scheduleDate = date
  }
  
  func scheduleEnabled(_ enabled:  Bool) {
    scheduleEnabled = enabled
    if !enabled {
      scheduleDate = nil
    }
  }
  
  private func add(_ task: Task) {
    taskDataStore.add(task)
  }
  
  private func update(_ task: Task) {
    taskDataStore.update(task)
  }
}
