//
//  AddEditTaskViewController.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 27/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import UIKit

class AddEditTaskViewController: UIViewController {
  
  @IBOutlet weak var taskTitleTextView: UITextView!
  @IBOutlet weak var onADayLabel: UILabel!
  @IBOutlet weak var scheduleEnableSwitch: UISwitch!
  @IBOutlet weak var dateTimePicker: UIDatePicker!
  
  private var doneBarButtonItem: UIBarButtonItem!

  var viewModel: AddEditTaskViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    title = viewModel.viewTitleText
    
    taskTitleTextView.becomeFirstResponder()
    viewModel.viewWillStartShowing()
  }
  
  //MARK:- Private
  private func setupView() {
    onADayLabel.font = .headingSecondary
    scheduleEnableSwitch.tintColor = .brandPrimary
    scheduleEnableSwitch.onTintColor = .brandPrimary
    
    onADayLabel.textColor = .textPrimary
    
    view.backgroundColor = .background
    
    dateTimePicker.setValue(UIColor.white, forKey: "textColor")
    dateTimePicker.sendAction(Selector(("setHighlightsToday:")), to: nil, for: nil)
    let date = Date()
    dateTimePicker.setDate(date, animated: false)
    dateTimePicker.minimumDate = date
    
    taskTitleTextView.backgroundColor = .background
    taskTitleTextView.textColor = .textPrimary
    taskTitleTextView.tintColor = .brandPrimary
    taskTitleTextView.font = .title
    taskTitleTextView.delegate = self
    
    
    let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(AddEditTaskViewController.outsideTapAction))
    view.addGestureRecognizer(tapGestureRecogniser)
    
    doneBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(AddEditTaskViewController.doneButtonAction))
    doneBarButtonItem.tintColor = .textPrimary
    
    let cancelButtonAction = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(AddEditTaskViewController.cancelButtonAction))
    cancelButtonAction.tintColor = .textPrimary
    
    navigationItem.rightBarButtonItem = doneBarButtonItem
    navigationItem.leftBarButtonItem = cancelButtonAction
    
    
    scheduleEnableSwitch.isOn = false
    dateTimePicker.isHidden = true
  }
  
  
  //MARK:- Actions
  
  @IBAction func scheduleSwitchValueChangeAction(_ sender: Any) {
    viewModel.scheduleEnabled(scheduleEnableSwitch.isOn)
    
    dateTimePicker.isHidden = !scheduleEnableSwitch.isOn
    if scheduleEnableSwitch.isOn {
      viewModel.scheduleDateUpdated(dateTimePicker.date)
    }
    
    UIView.animate(withDuration: 0.3) { [unowned self] in
      self.view.layoutIfNeeded()
    }
  }
  
  @IBAction func scheduleDateChangedAction(_ sender: Any) {
    viewModel.scheduleDateUpdated(dateTimePicker.date)
  }
  
  @objc private func outsideTapAction() {
    view.endEditing(true)
  }
  
  @objc private func doneButtonAction() {
    view.endEditing(true)
    viewModel.addOrUpdateTask()
    dismiss(animated: true, completion: nil)
  }
  
  @objc private func cancelButtonAction() {
    view.endEditing(true)
    dismiss(animated: true, completion: nil)
  }
}

//MARK:- Text View Delegate

extension AddEditTaskViewController: UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    viewModel.titleUpdated(textView.text)
  }
}

//MARK:- View protocol

extension AddEditTaskViewController: AddEditTaskViewProtocol {
  func updateTaskTitle(_ title: String) {
    taskTitleTextView.text = title
  }
  
  func enableSchedule(_ enable: Bool) {
    scheduleEnableSwitch.isOn = enable
  }
  
  func updateScheduleDate(_ date: Date) {
    dateTimePicker.isHidden = false
    dateTimePicker.setDate(date, animated: false)
  }
  
  func updateSave(enabled: Bool) {
    doneBarButtonItem.isEnabled = enabled
  }
}
