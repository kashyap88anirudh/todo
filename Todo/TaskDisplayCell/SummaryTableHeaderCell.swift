//
//  SummaryTableHeaderCell.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 26/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import UIKit

class SummaryTableHeaderCell: UITableViewHeaderFooterView {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var backgroundFillView: UIView!
  @IBOutlet weak var dividerView: UIView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    titleLabel.font = .headingPrimary
    titleLabel.textColor = .brandPrimary
    backgroundFillView.backgroundColor = .background
    dividerView.backgroundColor = .divider
  }
}
