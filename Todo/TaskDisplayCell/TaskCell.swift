//
//  TodoCell.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 26/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation
import UIKit

class TaskCell: UITableViewCell {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var scheduleLabel: UILabel!
  @IBOutlet weak var checkButton: UIButton!
  
  var completeActionCallback: (() -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    titleLabel.font = .title
    titleLabel.textColor = .textPrimary
    scheduleLabel.font = .title
    scheduleLabel.textColor = .textSecondary
  }
  
  func bind(_ viewModel: TaskCellViewModel) {
    titleLabel.text = viewModel.titleText
    scheduleLabel.text = viewModel.scheduleDateText
    
    if viewModel.isComplete {
      checkButton.setImage(UIImage(named: "completeButton"), for: .normal)
    } else {
      checkButton.setImage(UIImage(named: "notCompleteButton"), for: .normal)
    }
  }
  
  func animateCompletion(completion: @escaping () -> Void) {
    self.titleLabel.attributedText = NSAttributedString(string: self.titleLabel.text! , attributes: self.strikeThroughAttribute())
    self.checkButton.setImage(UIImage(named: "completeButton"), for: .normal)
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      completion()
    }
  }
  
  private func strikeThroughAttribute() -> [NSAttributedStringKey: Any]{
    let attributes = [NSAttributedStringKey.strikethroughStyle: NSUnderlineStyle.styleThick.rawValue,
                      NSAttributedStringKey.strikethroughColor: UIColor.brandPrimary] as [NSAttributedStringKey : Any]
    return attributes
  }
  
  //MARK:- Action
  @IBAction func completeAction() {
    completeActionCallback?()
  }

}
