//
//  TaskCellViewModel.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 27/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation

class TaskCellViewModel {
  private let task: Task
  
  var titleText: String {
    return task.title
  }
  
  var scheduleDateText: String? {
    if let date = task.schedule {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "dd/MM/YY HH:mm a"
      return dateFormatter.string(from: date)
    }
    
    return nil
  }
  
  var isComplete: Bool {
    return task.isComplete
  }
  
  init(task: Task) {
    self.task = task
  }
  
  
}
