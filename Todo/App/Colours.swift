//
//  Colours.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 25/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import UIKit

extension UIColor {
  class var brandPrimary: UIColor {
    return UIColor(r: 70.0, g: 200.0, b: 256.0, a: 1)
  }
  
  class var brandSecondary: UIColor {
    return UIColor(r: 254.0, g: 213.0, b: 0.0, a: 1)
  }
  
  class var background: UIColor {
    return UIColor(r: 22.0, g: 25.0, b: 30.0, a: 0.95)
  }
 
  class var textPrimary: UIColor {
    return .white
  }
  
  class var textSecondary: UIColor {
    return UIColor(r: 178.0, g: 178.0, b: 178.0, a: 1.0)
  }
  
  class var divider: UIColor {
    return UIColor(r: 49.0, g: 49.0, b: 49.0, a: 1.0)
  }
}


extension UIColor{
  convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) {
    self.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
  }
}
