//
//  Typeface.swift
//  Todo
//
//  Created by Anirudh Ramesh Kashyap on 26/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import UIKit

extension UIFont {
  class var headingPrimary: UIFont {
    return UIFont.systemFont(ofSize: 18.0, weight: .regular)
  }
  
  class var headingSecondary: UIFont {
    return UIFont.systemFont(ofSize: 16.0, weight: .regular)
  }
  
  class var title: UIFont {
    return UIFont.systemFont(ofSize:14.0, weight: .regular)
  }
}
