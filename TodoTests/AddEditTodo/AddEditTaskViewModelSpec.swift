//
//  AddEditTaskViewModelSpec.swift
//  TodoTests
//
//  Created by Anirudh Ramesh Kashyap on 27/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import Todo

class AddEditTaskViewModelSpec: QuickSpec {
  
  var sut: AddEditTaskViewModel!
  fileprivate var viewMock: AddEditTaskViewMock!
  fileprivate var mockDataStore: MockTaskDataStore!
  
  override func spec() {
    beforeEach {
      self.viewMock = AddEditTaskViewMock()
      self.mockDataStore = MockTaskDataStore()
      
      self.sut = AddEditTaskViewModel(taskDataStore: self.mockDataStore, viewProtocol: self.viewMock)
    }
    
    describe("Add Edit Task View Model") {
      describe("Save button") {
        context("when the title has no text") {
          it("will disable save button") {
            self.sut.titleUpdated("")
            expect(self.viewMock.saveEnabled).to(beFalsy())
          }
        }
        
        context("when the title has more than 1 character") {
          it("will enable save button") {
            self.sut.titleUpdated("Test")
            expect(self.viewMock.saveEnabled).to(beTruthy())
          }
        }
      }
    }
    
    describe("Save task") {
      context("when task is a new task") {
        it("will add task") {
          self.sut.titleUpdated("New Task")
          self.sut.addOrUpdateTask()
          expect(self.mockDataStore.isAddCalled).to(beTrue())
        }
      }
      
      context("when task is a exising task") {
        it("will update task") {
          let task = Task(title: "MockTitle", schedule: nil, isComplete: false, createdOn: Date())
          self.sut = AddEditTaskViewModel(taskDataStore: self.mockDataStore, viewProtocol: self.viewMock, taskToUpdate: task)
          self.sut.titleUpdated("New Task")
          self.sut.addOrUpdateTask()
          expect(self.mockDataStore.isUpdateCalled).to(beTrue())
        }
      }
    }
    
    
  }
}

private class AddEditTaskViewMock: AddEditTaskViewProtocol {

  private(set) var saveEnabled: Bool?
  
  func updateSave(enabled: Bool) {
    saveEnabled = enabled
  }
  
  func enableSchedule(_ enable: Bool) { }
  
  func updateScheduleDate(_ date: Date) { }
  
  func updateTaskTitle(_ title: String) { }
}

private class MockTaskDataStore: TaskDataStore {
  private(set) var isAddCalled = false
  private(set) var isUpdateCalled = false
  
  override func add(_ task: Task) {
    isAddCalled = true
  }
  
  override func update(_ task: Task) {
    isUpdateCalled = true
  }
}

