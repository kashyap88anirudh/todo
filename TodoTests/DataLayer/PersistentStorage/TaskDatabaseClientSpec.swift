//
//  TaskDatabaseClientSpec.swift
//  TodoTests
//
//  Created by Anirudh Ramesh Kashyap on 28/5/18.
//  Copyright © 2018 Anirudh Ramesh Kashyap. All rights reserved.
//

import Foundation
import Quick
import Nimble
import RealmSwift

@testable import Todo

class TaskDatabaseClientSpec: QuickSpec {
  
  var sut: TaskDatabaseClient!
  
  override func spec() {
    beforeEach {
      Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
      self.sut = TaskDatabaseClient()
    }
    
    describe("Task Database client") {
      
      context("adding an item") {
        it("will add it to the database"){
          let taskTitle = "MockTask"
          let creationDate = Date()
          let task = Task(title: taskTitle, schedule: nil, isComplete: false, createdOn: creationDate, taskID: "MockTaskID")
          self.sut.add(task: task)
          
          let taskRetreived = self.sut.allItems()?.first
          
          expect(self.sut.allItems()?.count).to(equal(1))
          expect(taskRetreived?.title).to(equal("MockTask"))
          expect(taskRetreived?.createdOn).to(equal(creationDate))
        }
      }
      
      context("when there are tasks scheduled for today and querying for tasks for today") {
        it("will return all tasks scheduled for today"){
          let creationDate = Date()
          let task = Task(title: "MockTask", schedule: creationDate, isComplete: false, createdOn: creationDate, taskID: "MockTaskID")
          self.sut.add(task: task)
          expect(self.sut.allItems()?.count).to(equal(1))
        }
        
        context("when the task is scheuled for the end of the day") {
          it("will return all tasks scheduled for today"){
            let scheduleDate = Date().endOfDay!
            let task = Task(title: "MockTask", schedule: scheduleDate, isComplete: false, createdOn: Date(), taskID: "MockTaskID")
            self.sut.add(task: task)
            expect(self.sut.allItems()?.count).to(equal(1))
          }
        }
        
      }
      
      context("querying for unscheduled items") {
        it("will return all tasks which are unscheduled"){
          let task = Task(title: "MockTask", schedule: nil, isComplete: false, createdOn:  Date(), taskID: "MockTaskID")
          let anotherTask = Task(title: "MockTask", schedule: nil, isComplete: false, createdOn:  Date(), taskID: "MockTaskID2")
          self.sut.add(task: task)
          self.sut.add(task: anotherTask)
          expect(self.sut.itemsWithNoSchedule()?.count).to(equal(2))
        }
      }
      
      context("querying for items scheduled after a particular date") {
        it("will return all tasks which are scheduled for later"){
          //Add a day
          let scheduleDate = Date().addingTimeInterval(60 * 60 * 24)
          let task = Task(title: "MockTask", schedule: scheduleDate, isComplete: false, createdOn: Date(), taskID: "MockTaskID")
          
          self.sut.add(task: task)
          
          //Add 2 days
          let anotherScheduleDate = Date().addingTimeInterval(60 * 60 * 24 * 2)
          let anotherTask = Task(title: "MockTask", schedule: anotherScheduleDate, isComplete: false, createdOn: Date(), taskID: "MockTaskID")
          self.sut.add(task: anotherTask)
          
          expect(self.sut.itemsScheduledAfter(date: Date())?.count).to(equal(2))
        }
      }
      
      context("querying for completed tasks") {
        it("will return all completed tasks"){
    
          let task = Task(title: "MockTask", schedule: nil, isComplete: true, createdOn: Date(), taskID: "MockTaskID")
          self.sut.add(task: task)
          expect(self.sut.completedItems()?.count).to(equal(1))
        }
      }
      
      context("updating task") {
        it("will update the task with right details") {
          let task = Task(title: "MockTask", schedule: nil, isComplete: true, createdOn: Date(), taskID: "MockTaskID")
          self.sut.add(task: task)
          
          var retrivedTask = self.sut.allItems()?.first
          retrivedTask?.title = "Updated Mock Title"
          self.sut.update(task: retrivedTask!)
          
          expect(self.sut.allItems()?.first?.title).to(equal("Updated Mock Title"))
          
        }
      }
      
    }
  }
}
